

#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

GLfloat x1 = 100.0f;
GLfloat y1 = 150.0f;
GLsizei rsize = 25;

GLfloat xstep = 1.0f;
GLfloat ystep = 1.0f;
GLfloat windowWidth;
GLfloat windowHeight;
void RenderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(x1,y1); //V0
	glVertex2f(x1-25.0f, y1-5.0f); //V1
	glVertex2f(x1-15.0f, y1-25.0f); //V2
	glVertex2f(x1+15.0f, y1-25.0f);//v3
	glVertex2f(x1+25.0f, y1-5.0f);
	glVertex2f(x1+20.0f, y1+20.0f);
	glVertex2f(x1+0.0f, y1+25.0f);
	glVertex2f(x1-20.0f, y1+20.0f);
	glVertex2f(x1-25.0f, y1-5.0f);



	glEnd();    
	glutSwapBuffers();
}
void TimerFunction(int value) {
	if (x1 > windowWidth - rsize || x1 < 0)
		xstep = -xstep;

	if (y1 > windowHeight - rsize || y1 < 0)
		ystep = -ystep;


	if (x1 > windowWidth - rsize)
		x1 = windowWidth - rsize - 1;

	if (y1 > windowHeight - rsize)
		y1 = windowHeight - rsize - 1;

	x1 += xstep;
	y1 += ystep; 
	glutPostRedisplay();
	glutTimerFunc(33, TimerFunction, 1);
} 
void SetupRC(void) {
	glClearColor(0.6f, 0.4f, 0.12f, 0.12f);
}
void ChangeSize(GLsizei w, GLsizei h) {
	if (h == 0)
		h = 1;

	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (w <= h)
	{
		windowHeight = 250.0f*h / w;
		windowWidth = 250.0f;
	}
	else
	{
		windowWidth = 250.0f*w / h;
		windowHeight = 250.0f;
	}

	glOrtho(0.0f, windowWidth, 0.0f, windowHeight, 1.0f, -1.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
void main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Bounce");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	glutTimerFunc(33, TimerFunction, 1);
	SetupRC();
	glutMainLoop();
}



