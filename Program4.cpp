// Program4.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


GLfloat rtri;         // K�t obrotu 

void RenderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0f, 0.0f, 0.0f);
	glRotatef(rtri, 0.0f, 1.0f, 0.0f);
	
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0.0f, 0.0f); //V0
	glVertex2f(-25.0f, -5.0f); //V1
	glVertex2f(-15.0f, -25.0f); //V2
	glVertex2f(15.0f, -25.0f);//v3
	glVertex2f(25.0f, -5.0f);
	glVertex2f(20.0f, 20.0f);
	glVertex2f(0.0f, 25.0f);
	glVertex2f(-20.0f, 20.0f);
	glVertex2f(-25.0f, -5.0f);

	glEnd();  
	glLoadIdentity();       
	glTranslatef(0.0f, 0.0f, 0.0f);         
	glRotatef(rtri, 0.0f, 0.5f, 0.0f);
	glutSwapBuffers();
}
void TimerFunction(int value) {
	glPushMatrix();
	rtri = rtri + 2;

	glutPostRedisplay();

	glutTimerFunc(33, TimerFunction, 1);

	glPopMatrix();
}

void SetupRC(void) {
	glClearColor(0.6f, 0.4f, 0.12f, 0.12f);
}
void ChangeSize(int w, int h) {
	GLfloat aspectRatio;
	if (h == 0)   h = 1;    
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 
	aspectRatio = (GLfloat)w / (GLfloat)h;
	if (w <= h)    glOrtho(-100.0, 100.0, -100 / aspectRatio, 100.0 / aspectRatio, 1.0, -1.0);
	else    glOrtho(-100.0 * aspectRatio, 100.0 * aspectRatio, -100.0, 100.0, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);  glLoadIdentity();
}
int main(int argc, char* argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutCreateWindow("GLRect");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);

	glutTimerFunc(33, TimerFunction, 1);
	SetupRC();
	glutMainLoop();
	return 0;
}



